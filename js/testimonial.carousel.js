/* ========================================================================
 * Testimonial Carousel: testimonial.carousel.js v1.0.0
 * http://adrian-morelos.com/javascript/#testimonial.carousel
 * Copyright 2017 Adrian Morelos.
 * ======================================================================== */

function var_dump($var, title) {
    if (title) {
        console.log('// ' + title);
    }
    console.log('// ======================');
    console.log($var);
    console.log('// ======================');
}

(function ($) {
    "use strict";

    // CSS TRANSITION SUPPORT
    // ======================
    // Extends the jQuery.support object to CSS3 transition
    function TransitionSupport() {
        // body element
        var body = document.body || document.documentElement,
            // transition events with names
            transEndEventNames = {
                'WebkitTransition': 'webkitTransitionEnd',
                'MozTransition': 'transitionend',
                'MsTransition': 'MSTransitionEnd',
                'OTransition': 'oTransitionEnd otransitionend',
                'transition': 'transitionend'
            }, name;

        for (name in transEndEventNames) {
            if (body.style[name] !== undefined) {
                // return object with transition end event name
                return {end: transEndEventNames[name]}
            }
        }
        // css transitions are not supported
        return false;
    }

    $(function () {
        $.support.testimonial_transition = TransitionSupport();
        if (!$.support.testimonial_transition) {
            return false;
        }
        // define alias Special event hook
        $.event.special.testimonialCarouselTransitionEnd = {
            bindType: $.support.testimonial_transition.end,
            delegateType: $.support.testimonial_transition.end,
            handle: function (e) {
                if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
            }
        }
    });

    // TESTIMONIAL CAROUSEL CLASS DEFINITION
    // =====================================

    var TestimonialCarousel = function (element, config) {
        this._element = $(element);
        this._activeElement = null;
        this._items = null;
        this._intervalId = null; // A Number, representing the ID value of the timer that is set on setInterval
        this._indicatorsElement = this._element.find(TestimonialCarousel.Selector.INDICATORS);

        this._config = config;
        this._isPaused = false;
        this._isSliding = false;

        // Setup pause on hover
        if (this._config.pause == 'hover') {
            $(this._element).on(TestimonialCarousel.Event.MOUSEENTER, $.proxy(this.pause, this)).on(TestimonialCarousel.Event.MOUSELEAVE, $.proxy(this.cycle, this));
        }
    };

    TestimonialCarousel.VERSION = '1.0.0';
    // transition speed
    TestimonialCarousel.TRANSITION_DURATION = 600;

    TestimonialCarousel.DEFAULTS = {
        intervalTime: 5000, // auto-slide interval
        pause: 'hover'// when to pause auto-slide
    };

    TestimonialCarousel.Selector = {
        ACTIVE: '.active',
        INDICATORS: '.testimonial-carousel-indicators',
        NEXT_PREV: '.prev, .next',
        ACTIVE_ITEM: '.testimonial-carousel-item.active',
        ITEM: '.testimonial-carousel-item'
    };

    TestimonialCarousel.ClassName = {
        ACTIVE: 'active'
    };

    TestimonialCarousel.Event = {
        MOUSEENTER: 'mouseenter',
        MOUSELEAVE: 'mouseleave',
        SLIDE: 'slide',
        SLID: 'slid'
    };

    TestimonialCarousel.Direction = {
        NEXT: 'next',
        PREV: 'prev',
        LEFT: 'left',
        RIGHT: 'right',
        FIRST: 'first',
        LAST: 'last'
    };

    TestimonialCarousel.prototype = {
        /** TestimonialCarousel.pause
         * --------------------------
         * Stops the carousel from cycling through items.
         * @param event
         * @return TestimonialCarousel
         */
        pause: function (event) {
            if (!event) {
                this._isPaused = true;
            }
            if ((this._element.find(TestimonialCarousel.Selector.NEXT_PREV).length) && $.support.testimonial_transition) {
                this.cycle(true);
            }
            clearInterval(this._intervalId);
            this._intervalId = null;
            return this;
        },
        /** TestimonialCarousel.next
         * -------------------------
         * Cycles to the next item.
         * @return TestimonialCarousel
         */
        next: function () {
            // return if already is sliding
            if (this._isSliding) {
                return this;
            }
            return this.slide(TestimonialCarousel.Direction.NEXT); // go to next item
        },
        /** TestimonialCarousel.prev
         * -------------------------
         * Cycles to the previous item.
         * @return TestimonialCarousel
         */
        prev: function () {
            // return if already is sliding
            if (this._isSliding) {
                return this;
            }
            return this.slide(TestimonialCarousel.Direction.PREV);// go to previous item
        },
        /** TestimonialCarousel.cycle
         * --------------------------
         * Start auto-cycle of items and Cycles through the carousel items from left to right.
         * @param event
         * @return TestimonialCarousel
         */
        cycle: function (event) {
            if (!event) {
                this._isPaused = false;
            }
            // Check for interval
            if (this._intervalId) {
                // Remove interval
                clearInterval(this._intervalId);
                this._intervalId = null;
            }
            // Setup new loop interval
            if (this._config.intervalTime && !this._isPaused) {
                this._intervalId = setInterval($.proxy(this.next, this), this._config.intervalTime);
            }
            return this;
        },
        /** TestimonialCarousel._getItemIndex
         * ----------------------------------
         * Navigate through the carousel items.
         * @param element
         * @return int
         */
        _getItemIndex: function (element) {
            this._items = element.parent().children(TestimonialCarousel.Selector.ITEM);
            return this._items.index(element || this._activeElement);
            //return $(element).index();
        },
        /** TestimonialCarousel.to
         * ----------------------------------
         * Navigate to specific item in the carousel.
         * @param element
         * @return int
         */
        to: function (selector) {
            if(!selector){
                return this;
            }
            var that = this;
            this._activeElement = this._element.find(TestimonialCarousel.Selector.ACTIVE_ITEM);
            var activeElementIndex = this._getItemIndex(this._activeElement);

            var toElement = this._element.find(selector);
            var toElementIndex = this._getItemIndex(toElement);

            if (toElementIndex > (this._items.length - 1) || toElementIndex < 0) {
                return this;
            }
            if (this._isSliding) {
                $(this._element).one(this.Event.SLID, function () {
                    return that.to(selector);
                });
                return this;
            }
            if (activeElementIndex === toElementIndex) {
                this.pause();
                this.cycle();
                return this;
            }
            var direction = toElementIndex > activeElementIndex ? TestimonialCarousel.Direction.NEXT : TestimonialCarousel.Direction.PREV;
            return this.slide(direction, this._items.eq(toElementIndex));
        },
        /** TestimonialCarousel.slide
         * --------------------------
         * Navigate through the carousel items.
         * @param type
         * @param element
         * @return TestimonialCarousel
         */
        slide: function (type, element) {
            var that = this; // self-references
            var activeElement = this._element.find(TestimonialCarousel.Selector.ACTIVE_ITEM);
            var nextElement = element || activeElement[type](); // next item to show
            var direction = (type == TestimonialCarousel.Direction.NEXT) ? TestimonialCarousel.Direction.LEFT : TestimonialCarousel.Direction.RIGHT; // direction of next item
            var fallback = (type == TestimonialCarousel.Direction.NEXT) ? TestimonialCarousel.Direction.FIRST : TestimonialCarousel.Direction.LAST; // fallback if next item not found
            var isCycling = Boolean(this._intervalId); // is there an auto-sliding?
            // if no items, try to find items
            if (!this._items) {
                this._items = this._element.find(TestimonialCarousel.Selector.ITEM);
                // if no items, do nothing
                if (!this._items.length) {
                    return this;
                }
            }
            nextElement = (nextElement && nextElement.length) ? nextElement : this._items[fallback](); // ensure next element
            if (!activeElement || !nextElement) {
                // some weirdness is happening, so we bail
                return this;
            }
            // return if next element is already active
            if (nextElement.hasClass(TestimonialCarousel.ClassName.ACTIVE)) {
                this._isSliding = false;
                return this;
            }
            // Slide Event
            var slideEvent = $.Event(TestimonialCarousel.Event.SLIDE, {
                relatedTarget: nextElement, // related target is next item to show
                direction: direction
            });
            // trigger slide event
            this._element.trigger(slideEvent);
            // return if the event was canceled
            if (slideEvent.isDefaultPrevented()) {
                return this;
            }
            // if auto-sliding, pause slide
            if (isCycling) {
                this.pause();
            }
            // Start Slide Process
            this._isSliding = true; // set sliding status to true
            // set Active Indicator Element
            if (this._indicatorsElement) {
                this._indicatorsElement.find(TestimonialCarousel.Selector.ACTIVE).removeClass(TestimonialCarousel.ClassName.ACTIVE);
                var nextIndicator = this._indicatorsElement.children().eq(this._getItemIndex(nextElement));
                if (nextIndicator) {
                    nextIndicator.addClass(TestimonialCarousel.ClassName.ACTIVE);
                }
            }

            // if css transition support
            if ($.support.testimonial_transition) {
                nextElement.addClass(type);// add type class to next item
                nextElement[0].offsetWidth // force re-flow Width on next item
                activeElement.addClass(direction);// add direction class to active item
                nextElement.addClass(direction);// add direction class to next item
                // invoke css transition
                activeElement.one($.support.testimonial_transition.end, function () {
                    nextElement.removeClass(type + ' ' + direction).addClass(TestimonialCarousel.ClassName.ACTIVE);
                    nextElement.removeAttr('aria-hidden');// update ARIA state
                    activeElement.removeClass(TestimonialCarousel.ClassName.ACTIVE + ' ' + type + ' ' + direction);
                    activeElement.attr('aria-hidden', true); // update ARIA state
                    that._isSliding = false;
                });
            } else {
                activeElement.removeClass(TestimonialCarousel.ClassName.ACTIVE);
                activeElement.attr('aria-hidden', true); // update ARIA state
                nextElement.addClass(TestimonialCarousel.ClassName.ACTIVE);
                nextElement.removeAttr('aria-hidden');// update ARIA state
                this._isSliding = false;
            }

            if (isCycling) {
                this.cycle(); // start/resume loop
            }
            // return reference to self for chaining
            return this;
        }

    };


    // TESTIMONIAL CAROUSEL PLUGIN DEFINITION
    // ======================================
    function TestimonialCarouselHandler(option) {
        return this.each(function () {
            var $testimonial_carousel = $(this);
            var options = $.extend({}, TestimonialCarousel.DEFAULTS, $testimonial_carousel.data());
            var testimonialCarousel = $testimonial_carousel.data('testimonial.carousel'); // Limits the number of instances of a particular object to just one.
            var operation = null;
            switch (typeof option) {
                case 'object':
                    options = $.extend({}, options, option);
                    if ('slide' in options) {
                        operation = options.slide;
                    } else if ('slideTo') {
                        operation = 'go_to';
                    }
                    break;
                case 'string':
                    operation = option;
                    break;
                default:
                    return false;
            }
            // Initialize the TestimonialCarousel only has not been initialized
            if (!testimonialCarousel) {
                testimonialCarousel = new TestimonialCarousel(this, options);
                $testimonial_carousel.data('testimonial.carousel', testimonialCarousel);
            }
            // Run operations
            if (operation == 'go_to') {
                testimonialCarousel.to(option.slideTo);
            } else if (operation) {
                if (operation in testimonialCarousel) {
                    testimonialCarousel[operation]();
                }
            } else if (options.interval) {
                // Start auto-slide by default if interval time is set
                testimonialCarousel.pause().cycle()
            }
        })
    }

    var old = $.fn.testimonial_carousel;

    $.fn.testimonial_carousel = TestimonialCarouselHandler;
    $.fn.testimonial_carousel.Constructor = TestimonialCarousel;

    // TESTIMONIAL CAROUSEL NO CONFLICT
    // ================================
    $.fn.testimonial_carousel.noConflict = function () {
        $.fn.testimonial_carousel = old;
        return this;
    };

    // TESTIMONIAL CAROUSEL CONTROLS
    // =============================
    var clickHandler = function (event) {
        var $this = $(this);
        var $target_testimonial_carousel = null;
        var data_target = $this.attr('data-target');
        if (typeof data_target !== typeof undefined && data_target !== false) {
            $target_testimonial_carousel = $(data_target);
        } else {
            data_target = $this.attr('href');
            if (typeof data_target !== typeof undefined && data_target !== false) {
                $target_testimonial_carousel = $(data_target);
            }
        }
        if (!$target_testimonial_carousel.hasClass('testimonial_carousel')) {
            return false;
        }
        var options = $.extend({}, $target_testimonial_carousel.data(), $this.data());

        TestimonialCarouselHandler.call($target_testimonial_carousel, options);

        event.preventDefault();
    };

    $(document).on('click.testimonial.carousel.controls', '[data-slide]', clickHandler).on('click.testimonial.carousel.controls', '[data-slide-to]', clickHandler);

    // INIT TESTIMONIAL CAROUSEL
    // =========================
    $(window).on('load', function () {
        $('[data-testimonial="carousel"]').each(function () {
            var $testimonial_carousel = $(this);
            TestimonialCarouselHandler.call($testimonial_carousel, $testimonial_carousel.data());
        })
    })

})(jQuery);

