<?php

/**
 * @file
 * Contains \Drupal\testimonial_field\Element\TestimonialCarousel.
 */

namespace Drupal\testimonial_field\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a Testimonial Carousel render element.
 *
 * Properties:
 * - #carousel_items: The Carousel Items .
 * Usage example:
 *
 * @code
 * $build['examples_testimonial_carousel'] = [
 *   '#type' => 'testimonial_carousel',
 *   '#carousel_items' => []
 * ];
 * @endcode
 *
 * @RenderElement("testimonial_carousel")
 */
class TestimonialCarousel extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'testimonial_carousel',
      '#carousel_items' => '',
      '#attached' => [
        'library' => [
          'testimonial_field/testimonial.carousel',
        ],
      ],
    ];
  }

}