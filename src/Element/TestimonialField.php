<?php

/**
 * @file
 * Contains \Drupal\testimonial_field\Element\TestimonialField.
 */

namespace Drupal\testimonial_field\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form element for tabular data.
 *
 * @FormElement("testimonial_field")
 */
class TestimonialField extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#settings' => [
        'picture_options' => [
          'file_extensions' => 'png gif jpg jpeg',
          'file_directory' => '[date:custom:Y]-[date:custom:m]',
          'max_filesize' => '',
          'max_resolution' => '',
          'min_resolution' => '',
          'alt_field' => 1,
          'alt_field_required' => 1,
          'title_field' => 0,
          'title_field_required' => 0,
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ],
      ],
      '#process' => [
        [$class, 'processTestimonialField'],
      ],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes Testimonials Carousel items form element.
   */
  public static function processTestimonialField(&$element, FormStateInterface $form_state, &$complete_form) {
    $field_settings = $element['#settings'];
    $values = (isset($element['#value']) && is_array($element['#value'])) ? $element['#value'] : [];
    $id = implode('-', $element['#parents']);
    $wrapper_id = "testimonial-field-items-$id-fieldset-wrapper";
    $element['#tree'] = TRUE;

    $testimonial_field = isset($values['testimonial_field']) ? $values['testimonial_field'] : [];
    $testimonial_name = isset($testimonial_field['testimonial_name']) ? $testimonial_field['testimonial_name'] : '';
    $testimonial_label = isset($testimonial_field['testimonial_label']) ? $testimonial_field['testimonial_label'] : '';
    $testimonial_image = isset($testimonial_field['testimonial_image']) ? $testimonial_field['testimonial_image'] : '';
    $defaults = [
      'fids' => $testimonial_image,
      'description' => '',
    ];
    $testimonial_blockquote = isset($testimonial_field['testimonial_blockquote']) ? $testimonial_field['testimonial_blockquote'] : '';
    $testimonial_state = isset($testimonial_field['testimonial_state']) ? $testimonial_field['testimonial_state'] : 'published';

    $element['testimonial_field'] = [
      '#type' => 'fieldset',
      '#title' => t('Testimonial Carousel item'),
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
      '#attributes' => ['class' => ['form-testimonial-field']],
      'testimonial_name' => [
        '#type' => 'textfield',
        '#title' => t('Profile Name'),
        '#default_value' => $testimonial_name,
      ],
      'testimonial_label' => [
        '#type' => 'textfield',
        '#title' => t('Profile Label'),
        '#default_value' => $testimonial_label,
      ],
      'testimonial_image' => [
        '#type' => 'managed_file',
        '#title' => t('Profile Picture'),
        '#upload_validators' => [
          'file_validate_extensions' => $field_settings['picture_options']['file_extensions'],
          'file_validate_size' => [25600000],
        ],
        '#description' => t('The Profile Picture of the slide.'),
        '#default_value' => $defaults,
      ],
      'testimonial_blockquote' => [
        '#type' => 'textarea',
        '#title' => t('Testimonial Blockquote'),
        '#description' => t('Testimonial description'),
        '#default_value' => $testimonial_blockquote,
      ],
      'testimonial_state' => [
        '#type' => 'select',
        '#title' => t('Testimonial State'),
        '#options' => [
          'published' => t('Published'),
          'unpublished' => t('Unpublished'),
        ],
        '#default_value' => $testimonial_state,
        '#description' => t('The state of the slide. The unpublished slide will be excluded from the testimonials carousel.'),
      ],
    ];

    // Add upload resolution validation.
    if ($field_settings['picture_options']['max_resolution'] || $field_settings['picture_options']['min_resolution']) {
      $element['testimonial_field']['testimonial_image']['#upload_validators']['file_validate_image_resolution'] = [
        $field_settings['picture_options']['max_resolution'],
        $field_settings['picture_options']['min_resolution'],
      ];
    }

    // If not using custom extension validation, ensure this is an image.
    $supported_extensions = ['png', 'gif', 'jpg', 'jpeg'];
    $extensions = isset($element['testimonial_field']['testimonial_image']['#upload_validators']['file_validate_extensions'][0]) ? $element['testimonial_field']['testimonial_image']['#upload_validators']['file_validate_extensions'][0] : implode(' ', $supported_extensions);
    $extensions = array_intersect(explode(' ', $extensions), $supported_extensions);
    $element['testimonial_field']['testimonial_image']['#upload_validators']['file_validate_extensions'][0] = implode(' ', $extensions);

    // Add mobile device image capture acceptance.
    $element['testimonial_field']['testimonial_image']['#accept'] = 'image/*';

    // Add properties.
    if (isset($field_settings['picture_options']['preview_image_style'])) {
      $element['testimonial_field']['testimonial_image']['#preview_image_style'] = $field_settings['picture_options']['preview_image_style'];
    }
    if (isset($field_settings['picture_options']['progress_indicator'])) {
      $element['testimonial_field']['testimonial_image']['#progress_indicator'] = $field_settings['picture_options']['progress_indicator'];
    }
    if (isset($field_settings['picture_options']['title_field'])) {
      $element['testimonial_field']['testimonial_image']['#title_field'] = $field_settings['picture_options']['title_field'];
    }
    if (isset($field_settings['picture_options']['title_field_required'])) {
      $element['testimonial_field']['testimonial_image']['#title_field_required'] = $field_settings['picture_options']['title_field_required'];
    }
    if (isset($field_settings['picture_options']['alt_field'])) {
      $element['testimonial_field']['testimonial_image']['#alt_field'] = $field_settings['picture_options']['alt_field'];
    }
    if (isset($field_settings['picture_options']['alt_field_required'])) {
      $element['testimonial_field']['testimonial_image']['#alt_fialt_field_requiredeld'] = $field_settings['picture_options']['alt_field_required'];
    }

    $form_state->setCached(FALSE);

    return $element;
  }

}