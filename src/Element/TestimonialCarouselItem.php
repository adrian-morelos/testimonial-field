<?php

/**
 * @file
 * Contains \Drupal\testimonial_field\Element\TestimonialCarouselItem.
 */

namespace Drupal\testimonial_field\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a Testimonial Carousel Item render element.
 *
 * Properties:
 * - #testimonial_name: The Profile Name.
 * - #testimonial_label: The Profile Label.
 * - #testimonial_image: The Profile Picture.
 * - #testimonial_blockquote: The Testimonial Blockquote.
 * - #testimonial_state: The Testimonial State.
 * Usage example:
 *
 * @code
 * $build['examples_testimonial_carousel_item'] = [
 *   '#type' => 'testimonial_carousel_item',
 *   '#testimonial_name' => $this->t('Examples'),
 *   '#testimonial_label' => $this->t('Examples'),
 *   '#testimonial_image' => [],
 *   '#testimonial_state' => 'published',
 * ];
 * @endcode
 *
 * @RenderElement("testimonial_carousel_item")
 */
class TestimonialCarouselItem extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderTestimonialCarouselItem'],
      ],
      '#theme' => 'testimonial_carousel_item',
      '#testimonial_name' => '',
      '#testimonial_label' => '',
      '#testimonial_image' => [],
      '#testimonial_blockquote' => '',
      '#testimonial_state' => 'published',
    ];
  }

  /**
   * Render API callback.
   */
  public static function preRenderTestimonialCarouselItem($element) {
    if ($element['#testimonial_state'] != 'published') {
      hide($element);
    }
    else {
      show($element);
    }
    return $element;
  }

}