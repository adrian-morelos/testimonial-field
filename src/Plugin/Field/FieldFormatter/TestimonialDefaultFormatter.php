<?php

namespace Drupal\testimonial_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Element;


/**
 * Plugin implementation of the 'testimonial_default' formatter.
 *
 * @FieldFormatter(
 *   id = "testimonial_default",
 *   label = @Translation("Testimonial Carousel"),
 *   field_types = {
 *     "testimonial"
 *   }
 * )
 */
class TestimonialDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        // Implement default settings.
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        // Implement settings form.
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $field = $items->getFieldDefinition();
    $field_name = $field->getName();
    $field_settings = $field->getSettings();

    $entity = $items->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();

    $elements = [];
    $carousel_items = [];

    foreach ($items as $delta => $item) {
      if (!empty($item->values)) {
        $values = $item->values;
        $testimonial_field = isset($values['testimonial_field']) ? $values['testimonial_field'] : [];
        $testimonial_name = isset($testimonial_field['testimonial_name']) ? $testimonial_field['testimonial_name'] : '';
        $testimonial_label = isset($testimonial_field['testimonial_label']) ? $testimonial_field['testimonial_label'] : '';
        $testimonial_image = isset($testimonial_field['testimonial_image']) ? $testimonial_field['testimonial_image'] : '';
        $testimonial_blockquote = isset($testimonial_field['testimonial_blockquote']) ? $testimonial_field['testimonial_blockquote'] : '';
        $testimonial_state = isset($testimonial_field['testimonial_state']) ? $testimonial_field['testimonial_state'] : 'published';
        $carousel_items[$delta] = [
          '#type' => 'testimonial_carousel_item',
          '#testimonial_name' => $testimonial_name,
          '#testimonial_label' => $testimonial_label,
          '#testimonial_image' => $testimonial_image,
          '#testimonial_blockquote' => $testimonial_blockquote,
          '#testimonial_state' => $testimonial_state,
        ];
        $carousel_items[$delta]['#attributes']['class'][] = 'testimonial-carousel-item';
        if($delta == 0){
          $carousel_items[$delta]['#attributes']['class'][] = 'active';
        }
        $carousel_items[$delta]['#attributes']['id'] = "testimonial-carousel-item-$entity_type-$entity_id-$delta";
        $carousel_items[$delta]['id'] = "testimonial-carousel-item-$entity_type-$entity_id-$delta";

      }
    }
    $elements[] = [
      '#type' => 'testimonial_carousel',
      '#carousel_items' => $carousel_items,
      '#attributes' => [
        'id' => "testimonial-carousel-item-$entity_type-$entity_id",
      ]
    ];

    $elements['#attributes']['class'][] = 'testimonial-carousel-field-container';

    return $elements;
  }

}
