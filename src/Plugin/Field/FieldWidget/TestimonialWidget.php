<?php

namespace Drupal\testimonial_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'testimonial' widget.
 *
 * @FieldWidget(
 *   id = "testimonial",
 *   label = @Translation("Testimonial Widget Field"),
 *   field_types = {
 *     "testimonial"
 *   }
 * )
 */
class TestimonialWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'picture_options' => [
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ],
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $picture_options = $this->getSetting('picture_options');
    $element['picture_options'] = [
      '#type' => 'container',
      'preview_image_style' => [
        '#title' => t('Preview image style'),
        '#type' => 'select',
        '#options' => image_style_options(FALSE),
        '#empty_option' => '<' . t('no preview') . '>',
        '#default_value' => $picture_options['preview_image_style'],
        '#description' => t('The preview image will be shown while editing the content.'),
        '#weight' => 15,
      ],
      'progress_indicator' => [
        '#type' => 'radios',
        '#title' => t('Progress indicator'),
        '#options' => [
          'throbber' => t('Throbber'),
          'bar' => t('Bar with progress meter'),
        ],
        '#default_value' => $picture_options['progress_indicator'],
        '#description' => t('The throbber display does not show the status of uploads but takes up less space. The progress bar is helpful for monitoring progress on large uploads.'),
        '#weight' => 16,
        '#access' => file_progress_implementation(),
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $picture_options = $this->getSetting('picture_options');
    $summary = [];
    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $picture_options['preview_image_style'];
    if (isset($image_styles[$image_style_setting])) {
      $preview_image_style = t('Preview image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $preview_image_style = t('No preview');
    }
    $summary[] = $preview_image_style;
    $summary[] = t('Progress indicator: @progress_indicator', ['@progress_indicator' => $picture_options['progress_indicator']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_settings = $this->getFieldSettings();
    $picture_options = $this->getSetting('picture_options');
    if (isset($field_settings['picture_options'])) {
      $field_settings['picture_options'] += $picture_options;
    }

    ksm($items[$delta]);
    $default_values = isset($items[$delta]->values) ? $items[$delta]->values : [];
    $element['testimonial_item'] = [
      '#type' => 'testimonial_field',
      '#default_value' => $default_values,
      '#settings' => $field_settings,
    ];
    return $element;
  }

}
